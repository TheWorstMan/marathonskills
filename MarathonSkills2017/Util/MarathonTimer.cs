﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;

namespace MarathonSkills2017.Util
{
    public class MarathonTimer : UserControl, INotifyPropertyChanged
    {
        public string Time
        {
            get
            {
                return getTime();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        public void StartTimer()
        {
            Timer tmr = new Timer(1000);

            tmr.Elapsed += Tmr_Elapsed;

            tmr.Start();
        }

        private void Tmr_Elapsed(object sender, ElapsedEventArgs e)
        {
            OnPropertyChanged("Time");
        }

        public string getTime()
        {
            DateTime dtNow = DateTime.Now;
            DateTime dtTarget = DateTime.Parse("2017-10-10 06:00:00");

            TimeSpan ts = (dtTarget - dtNow);

            return string.Format("До старта: {0} дн. {1} ч. {2} мин {3} сек", ts.Days, ts.Hours, ts.Minutes, ts.Seconds);
        }
    }
}
