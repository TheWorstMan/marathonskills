﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace MarathonSkills2017.Util
{
    public static class Utility
    {
        private static marathonskillsEntities _database;
        public static marathonskillsEntities dBase
        {
            get
            {
                if (_database == null)
                    _database = new marathonskillsEntities();
                return _database;
            }
        }
        public static void CreateWindow(Type wnd, Window owner = null, bool showModel = false, bool close = false, params object[] param)
        {
            Window _wnd = (Window)Activator.CreateInstance(wnd, param);

            if (owner != null)
            {
                _wnd.Owner = owner;
                _wnd.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            }

            if (showModel)
                _wnd.ShowDialog();
            else
                _wnd.Show();

            _wnd.Owner = null;


            if(close && owner != null)
                owner.Close();
        }
        public static void CloseConnection()
        {
            if (_database != null)
                _database.Database.Connection.Close();
        }

        public static bool IsCorrectPassword(string text)
        {
            Regex reg = new Regex(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[\d])(?=.*?[!@#$%^]).{6,}$");

            return reg.IsMatch(text);
        }

        public static bool IsCorrectEmail(string text)
        {
            Regex reg = new Regex(@"[A-Za-z0-9]+@[A-Za-z0-9]+\.[A-Za-z0-9]+");

            return reg.IsMatch(text);
        }

        public static bool IsCorrectDate(DateTime dt)
        {
            return DateTime.Now.Subtract(dt).TotalDays > 3650;
        }

        public static ImageSource getImageFromFile(string path)
        {
            return new BitmapImage(new Uri(path));
        }
    }
}
