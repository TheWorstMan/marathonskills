﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MarathonSkills2017
{
    /// <summary>
    /// Interaction logic for wndAuth.xaml
    /// </summary>
    public partial class wndAuth : Window
    {
        public wndAuth()
        {
            InitializeComponent();
        }

        private void buttonAuth_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(textBox.Text))
            {
                return;
            }

            if (string.IsNullOrEmpty(textBox1.Text))
                return;

            var user = Util.Utility.dBase.User.SingleOrDefault(u => u.Email == textBox.Text && u.Password == textBox1.Text);

            if(user == null)
            {
                MessageBox.Show("Пользователь не найден!");
                return;
            }

            switch(user.RoleId)
            {
                case "R":
                    {
                        Util.Utility.CreateWindow(typeof(Menu.wndRunnerMenu), this, close: true);
                        break;
                    }
                case "A":
                    {
                        Util.Utility.CreateWindow(typeof(Menu.wndAdministratorMenu), this, close: true);
                        break;
                    }
                case "C":
                    {
                        Util.Utility.CreateWindow(typeof(Menu.wndCoordinatorMenu), this, close: true);
                        break;
                    }
            }
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            Util.Utility.CreateWindow(typeof(MainWindow), this, close: true);
        }

        private void buttonBack_Click(object sender, RoutedEventArgs e)
        {
            Util.Utility.CreateWindow(typeof(MainWindow), this, close: true);
        }
    }
}
