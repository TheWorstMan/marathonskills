﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MarathonSkills2017
{
    /// <summary>
    /// Interaction logic for BaseWindow.xaml
    /// </summary>
    public partial class BaseWindow : UserControl, INotifyPropertyChanged
    {
        public string title
        {
            get
            {
                return (string)GetValue(TitleProperty);
            }
            set
            {
                SetValue(TitleProperty, value);
                OnPropertyChanged("title");
            }
        }

        // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("title", typeof(string), typeof(BaseWindow), new PropertyMetadata("Marathon Skills 2017"));

        public string BackButton
        {
            get { return (string)GetValue(BackButtonProperty); }
            set { SetValue(BackButtonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BackButton.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BackButtonProperty =
            DependencyProperty.Register("BackButton", typeof(string), typeof(BaseWindow), new PropertyMetadata(null));



        public bool ShowTitle
        {
            get { return (bool)GetValue(ShowTitleProperty); }
            set { SetValue(ShowTitleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowTitle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowTitleProperty =
            DependencyProperty.Register("ShowTitle", typeof(bool), typeof(BaseWindow), new PropertyMetadata(true));


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        public BaseWindow()
        {
            this.DataContext = this;

            InitializeComponent();

            this.Loaded += delegate
            {
                Util.MarathonTimer tmr = new Util.MarathonTimer();
                textBlock.DataContext = tmr;
                tmr.StartTimer();

                buttonBack.Visibility = string.IsNullOrEmpty(BackButton) ? Visibility.Hidden : Visibility.Visible;

                rectangleTitle.Visibility = ShowTitle ? Visibility.Visible : Visibility.Hidden;
                Title.Visibility = ShowTitle ? Visibility.Visible : Visibility.Hidden;
            };
        }

        private void buttonBack_Click(object sender, RoutedEventArgs e)
        {
            Type type = Type.GetType(BackButton);

            //Util.Utility.CreateWindow(type, , close: true);
        }
    }
}
