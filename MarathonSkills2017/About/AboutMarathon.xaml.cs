﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MarathonSkills2017.About
{
    /// <summary>
    /// Interaction logic for AboutMarathon.xaml
    /// </summary>
    public partial class AboutMarathon : Window
    {
        public AboutMarathon()
        {
            InitializeComponent();
        }

        private void buttonBack_Click(object sender, RoutedEventArgs e)
        {
            Util.Utility.CreateWindow(typeof(wndAbout), this, close: true);
        }
    }
}
