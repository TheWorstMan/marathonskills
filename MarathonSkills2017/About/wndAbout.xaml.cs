﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MarathonSkills2017.About
{
    /// <summary>
    /// Interaction logic for wndAbout.xaml
    /// </summary>
    public partial class wndAbout : Window
    {
        public wndAbout()
        {
            InitializeComponent();
        }

        private void buttonBack_Click(object sender, RoutedEventArgs e)
        {
            Util.Utility.CreateWindow(typeof(MainWindow), this, close: true);
        }

        private void buttonAboutEvent_Click(object sender, RoutedEventArgs e)
        {
            Util.Utility.CreateWindow(typeof(AboutMarathon), this, close: true);
        }
    }
}
