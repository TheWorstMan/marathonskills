﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MarathonSkills2017.BecomeRunner
{
    /// <summary>
    /// Interaction logic for wndCheckRunner.xaml
    /// </summary>
    public partial class wndCheckRunner : Window
    {
        public wndCheckRunner()
        {
            InitializeComponent();
        }

        private void buttonBack_Click(object sender, RoutedEventArgs e)
        {
            Util.Utility.CreateWindow(typeof(MainWindow), this, close: true);
        }

        private void buttonBefore_Click(object sender, RoutedEventArgs e)
        {
            Util.Utility.CreateWindow(typeof(wndAuth), this, close: true);
        }

        private void buttonNewbie_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Данный функционал будет позже");
        }

        private void buttonLogin_Click(object sender, RoutedEventArgs e)
        {
            Util.Utility.CreateWindow(typeof(wndAuth), this, close: true);
        }
    }
}
