﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MarathonSkills2017.Menu
{
    /// <summary>
    /// Interaction logic for wndAdministratorMenu.xaml
    /// </summary>
    public partial class wndAdministratorMenu : Window
    {
        public wndAdministratorMenu()
        {
            InitializeComponent();
        }

        private void buttonBack_Click(object sender, RoutedEventArgs e)
        {
            Util.Utility.CreateWindow(typeof(wndAuth), this, close: true);
        }

        private void buttonLogout_Click(object sender, RoutedEventArgs e)
        {
            Util.Utility.CreateWindow(typeof(wndAuth), this, close: true);

        }
    }
}
