﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;



namespace MarathonSkills2017
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, RoutedEventArgs e)
        {
            Util.Utility.CreateWindow(typeof(wndAuth), this, close: true);
        }

        private void buttonAbout_Click(object sender, RoutedEventArgs e)
        {
            Util.Utility.CreateWindow(typeof(About.wndAbout), this, close: true);
        }

        private void buttonBecomeRunner_Click(object sender, RoutedEventArgs e)
        {
            Util.Utility.CreateWindow(typeof(BecomeRunner.wndCheckRunner), this, close: true);
        }
    }
}
