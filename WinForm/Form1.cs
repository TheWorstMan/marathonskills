﻿using AForge.Video.DirectShow;
using Puma.Net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tesseract;

namespace WinForm
{
    public partial class Form1 : Form
    {
        FilterInfoCollection captureDevice = new FilterInfoCollection(FilterCategory.VideoInputDevice);
        VideoCaptureDevice FinalFrame;

        public Form1()
        {
            InitializeComponent();

            comboBox1.DataSource = new UserCollection();
            comboBox1.DisplayMember = "Result";

            comboBox1.SelectedIndexChanged += (s, e) =>
            {
                if (comboBox1.SelectedItem == null)
                    return;

                DAL.User user = comboBox1.SelectedItem as DAL.User;
                richTextBox1.Clear();
                richTextBox1.AppendText(string.Format("Фамилия: {0}\nИмя: {1}\nДата р: {2}\nEmail: {3}\nRegID: {4}\nStatus: {5}",
                    user.LastName,
                    user.FirstName,
                    user.Runner.First().DateOfBirth,
                    user.Email,
                    user.Runner.First().Registration.First().RegistrationId,
                    user.Runner.First().Registration.First().RegistrationStatus.RegistrationStatus1));
            };

            this.Controls.Add(new MyButton());


            foreach (FilterInfo dev in captureDevice)
                comboBox2.Items.Add(dev.Name);

            FinalFrame = new VideoCaptureDevice();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox2.SelectedIndex >= 0)
            {
                FinalFrame = new VideoCaptureDevice(captureDevice[comboBox2.SelectedIndex].MonikerString);
                FinalFrame.NewFrame += FinalFrame_NewFrame;
                FinalFrame.Start();
            }
        }

        private void FinalFrame_NewFrame(object sender, AForge.Video.NewFrameEventArgs eventArgs)
        {
            pictureBox1.Image = (Bitmap)eventArgs.Frame.Clone();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (FinalFrame.IsRunning)
                FinalFrame.Stop();

            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "result.txt");
            if (File.Exists(path))
                File.Delete(path);

            StreamWriter sw = new StreamWriter(path);

            using (TesseractEngine ocr =
                new TesseractEngine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tessdata"),
                "eng+rus"))
            {
                using(var p = ocr.Process((Bitmap)pictureBox1.Image))
                {
                    sw.WriteLine(p.GetText() + "\n\n\n");
                }
            }


            PumaPage image = new PumaPage((Bitmap)pictureBox1.Image);

            using (image)
            {
                image.FileFormat = PumaFileFormat.RtfAnsi;
                image.AutoRotateImage = true;
                image.EnableSpeller = false;
                image.RecognizeTables = true;
                image.Language = PumaLanguage.Russian;
                
                try
                {
                    sw.WriteLine(image.RecognizeToString() + "\n\n\n");

                    Process.Start(path);
                }
                catch(Exception ex)
                {
                    image.Dispose();
                    Console.WriteLine(ex.Message.ToString());
                }
                finally
                {
                    sw.Close();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = "*.jpg;*.png;*.jpeg;*.gif;*.bmp|*.jpg;*.png;*.jpeg;*.gif;*.bmp";

            if(ofd.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = (Bitmap)Image.FromFile(ofd.FileName);
            }
        }
    }

    public class UserCollection : List<DAL.User>
    {
        public UserCollection()
        {
            DAL.MarathonEntities dBase = new DAL.MarathonEntities();
            dBase.User.ToList().ForEach(u => this.Add(u));
        }
    }

    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public int Id { get; set; }

        public string Result
        {
            get
            {
                return string.Format("{0}, {1} - {2}", LastName, FirstName, Country);
            }
        }
    }
}
