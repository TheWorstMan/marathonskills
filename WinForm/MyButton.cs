﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinForm
{
    class MyButton : Button
    {
        public MyButton()
        {
            Height = 40;
            Width = 40;
            Location = new Point(50, 350);
            //BackColor = Color.FromArgb(255, 128, 255);
        }

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            MessageBox.Show("test");
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            //base.OnPaint(pevent);
            pevent.Graphics.FillRectangle(new SolidBrush(Color.FromName("Control")), new Rectangle(0, 0, 40, 40));
            pevent.Graphics.FillEllipse(Brushes.BlueViolet, new Rectangle(0, 0, 40, 40));
  
        }
    }
}
